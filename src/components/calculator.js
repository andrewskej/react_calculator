import React, {Component} from 'react'


class Calculator extends Component{

//한번씩계산 & 실시간 계산은 된다. 
//이제 ver3. 한번에 연산식 여러개 쓰는걸 연구...사칙연산 우선순위도 있음..
//숫자랑 연산식을 리셋하지 말고 계속 담아야...

    state = {
        input:[],       //입력값
        waitingNum:'',  //다음 계산을 위한 대기숫자
        operators:[],   //연산자
        allEquation:[], //전체 공식
        result:'',      //들어온 순서대로만 계산한 값(사칙연산 우선순위 무시)

        result2:''      //testing, 사칙연산 우선순위 계속 적용하는 값(현재 에러가 많음)
    } 

    add = a => b => a+b;
    sub = a => b => a-b;
    mul = a => b => a*b;
    div = a => b => a/b;

    //입력시
    inputValue = e => {  
        let newInput = e.target.value;
        console.log('newinput:', newInput)
        this.setState({
                        input:[...this.state.input, newInput],
                        allEquation:[...this.state.allEquation, newInput]
                    }) 
    } 
    
    operator = e => { 
        let oper = e.target.value; 
        // this.setState({operators:[...this.state.operators, oper], allEquation:[...this.state.allEquation, oper]})
        this.setState({ operators:[oper], allEquation:[...this.state.allEquation, oper] })

        if(this.state.result!==''){ //이미 답이 있으면(계산을 한번이라도 했으면) - result 에 적용하면 되잖아
            this.setState({ waitingNum:[this.state.result], input:[] }) //첫 결과를 대기열에 올린다
        }else{ //첫 계산시
            let firstNum = this.state.input.reduce((acc,cur) => (acc+cur)) 
            this.setState({ waitingNum:[...this.state.waitingNum, Number(firstNum)], input:[] }) //숫자 준비해서 모으고 인풋 비우기
        }

        // if(this.state.result2!==''){ 
        //     this.setState({ waitingNum:[this.state.result2], input:[] }) //첫 결과를 대기열에 올린다
        // }else{ //첫 계산시
        //     let firstNum = this.state.input.reduce((acc,cur) => (acc+cur)) 
        //     this.setState({ waitingNum:[...this.state.waitingNum, Number(firstNum)], input:[] }) //숫자 준비해서 모으고 인풋 비우기
        // }
    }
   
   

    componentDidUpdate(){
        // //wip 연산자 우선순위를 계속 적용... 현재 한번밖에 안됨(첫 답만 맞음) + 첫연산에 곱셈나눗셈 들어가면 작동안함...
        console.log('waiting',this.state.waitingNum)
        console.log('oper:',this.state.operators)
        console.log(this.state.allEquation)
        let allEq = this.state.allEquation.reduce((acc,cur)=>{return acc+cur},'')
        console.log(allEq)

        var left;
        var addSubPrep;
        var divMulPrep;
        var divMulDone;
        if(allEq.match(/[/]/)!=null || allEq.match(/[*]/)!=null){
            if(allEq.match(/[/]/)!=null){ //곱하기가 아닐 때(나누기일 때)   14 + 6 /2
                left = allEq.split('/')[0] //나누기 왼쪽의 공식   14 + 6
                divMulPrep = this.div(Number(left.split('+')[1]))  //6 /
            }else if(allEq.match(/[*]/)!=null){ //나누기가 아닐 때(곱하기일 때?)
                left = allEq.split('*')[0] 
                divMulPrep = this.mul(Number(left.split('+')[1]))
            }
            if(left.match(/[+]/)!=null || left.match(/[-]/)!=null){
                if(left.match(/[+]/)!=null){
                    addSubPrep = this.add(Number(left.split('+')[0])) //14 + 
                }else if(left.match(/[-]/)!=null){
                    addSubPrep = this.sub(Number(left.split('+')[0]))
                }
                divMulDone = divMulPrep(Number(this.state.input.reduce((acc,cur)=>{return acc+cur},'')))
                let result2 = addSubPrep(divMulDone)
                console.log('result2',result2)
                if(this.state.result2 != result2){
                    this.setState({result2:result2})
                }
            }
        }
        //


        //들어온 순서대로 계산하는것...이건 일단 되는데
        let firstCalculation;
        switch(this.state.operators[0]){ 
            case '+': firstCalculation = this.add(Number(this.state.waitingNum[0]))
            break;
            case '-': firstCalculation = this.sub(Number(this.state.waitingNum[0]))
            break;
            case '*': firstCalculation = this.mul(Number(this.state.waitingNum[0]))
            break;
            case '/': firstCalculation = this.div(Number(this.state.waitingNum[0]))
            break;
        }

        if(this.state.waitingNum.length > 0 && this.state.input.length > 0){ //첫숫자 받았고 새입력이 들어가면
          let secondNum = this.state.input.reduce((acc,cur) => acc+cur)    //새입력을 모아서
          secondNum = Number(secondNum)                                       //모은 string을 숫자화
          
          let result= firstCalculation(secondNum)  
          console.log('result',result)  //<- 이값이 올라가야돼!
            if(result !== this.state.result){ //계산 결과값이 기존것과 다르면(변하면)
                  this.setState({result: result})  //업데이트
            }
        }

    }
    
    reset = () =>{
        this.setState({input:[], waitingNum:'', operators:[], result:'', result2:'', allEquation:[]})
    }
    
    //최종계산
    getResult = () =>{ // '=' 을 누르면
      this.setState({result:this.state.result})
    }



    render(){
        let calculatorStyle={
            'marginTop':'18%',
            'marginLeft':'18%'
        }

        let buttonStyle={
            'height':'70px',
            'width':'70px',
            'fontSize':'25px',
            'border':'solid black 1px',
            'background':'white'
        }

        let buttonStyle2={
            'height':'52.5px',
            'width':'52.5px',
            'fontSize':'25px',
            'border':'solid gray 1px',
            'background':'lightgray'
        }


        return(
            <div style={calculatorStyle}>
                calculator
                <div className="numbers" >
                    <div> {[7,8,9].map((el,i) => <button style={buttonStyle} key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                    <div> {[4,5,6].map((el,i) => <button style={buttonStyle} key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                    <div> {[1,2,3].map((el,i) => <button style={buttonStyle} key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                  
                    <button style={buttonStyle} onClick={this.inputValue} value=".">.</button>
                    <button style={buttonStyle} onClick={this.inputValue} value="0">0</button>
                    <button style={buttonStyle} onClick={this.reset}>c</button>
                </div>

                <div className="operators"> 
                    <div>{['+','-','*','/'].map((el,i)=><button style={buttonStyle2} key={i} value={el}  onClick={this.operator}>{el}</button>)}</div>
                    <button style={buttonStyle2} className="getResult" onClick={this.getResult}>=</button>
                </div>

                <div> {this.state.waitingNum} {this.state.operators}{this.state.input}  -> {this.state.result} </div>
           
                <div>
                    equation: {this.state.allEquation}
                </div>
                <div>
                     result: {this.state.result}
                </div>
                <div>
                     result2: {this.state.result2}
                </div>
            </div>
        )
    }


}

export default Calculator;