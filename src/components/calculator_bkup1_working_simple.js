import React, {Component} from 'react'


class Calculator extends Component{

//한번씩 연속 계산까지만 되는 첫버전

    state = {
        input:[],
    }

    add = a => b => Number(a) + Number(b)
    sub = a => b => Number(a) - Number(b)
    mul = a => b => Number(a) * Number(b)
    div = a => b => Number(a) / Number(b)

    inputValue = e =>{    
        let newInput = e.target.value
        this.setState({input:[...this.state.input, newInput]})
        let operators =['+','-','*','/']
        if(newInput.find(operators)){
            console.log(newInput)
        }
    } 
    
    getResult = () => {
        let input = this.state.input
        let operator = input.filter(el => el % 1 !== 0 && el !== '.')[0]
        let numCollect = input.reduce((acc,cur) => acc+cur)
        // console.log(numCollect)
        let firstInput = numCollect.split(operator)[0]
        // console.log(firstInput)
        let secondInput = numCollect.split(operator)[1]
        // console.log(secondInput)
        let result;

        switch(operator){
            case '+': result = this.add(firstInput)(secondInput)
                break;
            case '-': result = this.sub(firstInput)(secondInput)
                break;
            case '*': result = this.mul(firstInput)(secondInput)
                break;
            case '/': result = this.div(firstInput)(secondInput)
                break;
        }

        console.log('result',result)
        result = String(result).split('')


        this.setState({
            input:result
        })
    }

    reset = () =>{
        this.setState({input:''})
    }

    componentDidUpdate(){
        console.log(this.state.input)
    }
    
    
    render(){
        var calculatorStyle={
        }


        return(
            <div style={calculatorStyle}>
                calculator
                <div className="numbers" >
                    <div> {[7,8,9].map((el,i) => <button key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                    <div> {[4,5,6].map((el,i) => <button key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                    <div> {[1,2,3].map((el,i) => <button key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                  
                    <button onClick={this.inputValue} value=".">.</button>
                    <button onClick={this.inputValue} value="0">0</button>
                    <button onClick={this.reset}>c</button>
                </div>

                <div className="operators">
                    <div>{['+','-','*','/'].map((el,i)=><button key={i} value={el}  onClick={this.inputValue}>{el}</button>)}</div>
                    <button className="getResult" onClick={this.getResult}>=</button>
                </div>

                <div>{this.state.input}</div>
                
            </div>
        )
    }


}

export default Calculator;