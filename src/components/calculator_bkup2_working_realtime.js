import React, {Component} from 'react'


class Calculator extends Component{

//실시간계산 & 연속계산 구현 버전....

    state = {
        input:[],
        numPrep:'',  //연산 대기열
        operators:[],
        result:''   
    } 


    //currying 을 ...?
    add = a => b => a+b;
    sub = a => b => a-b;
    mul = a => b => a*b;
    div = a => b => a/b;

    //숫자 입력시
    inputValue = e => {  
        let newInput = e.target.value;
        this.setState({input:[...this.state.input, newInput]}) //입력한 숫자를 순서대로 넣는다 
    } 
    
    //연산자 입력시...두번째 연산시 numberPrep[0]는 사라져야..
    operator = e => { //연산자를 입력하는 순간 중간계산 해두기.. 우선 더하기만이라도 구현해보자 됐다 이제 나머지도...
        let oper = e.target.value; 
        // this.setState({operators:[...this.state.operators, oper]})
        this.setState({operators:[oper]})
        
        //분기 나누니까 된다!!!!ㅋㅋㅋㅋㅋ
        if(this.state.result!==''){ //이미 답이 있으면(계산을 한번이라도 했으면) - result 에 적용하면 되잖아
            this.setState({numPrep:[this.state.result], input:[]}) //첫 결과를 대기열에 올린다
        }else{ //첫 계산시
            let num1 = this.state.input.reduce((acc,cur) => Number(acc+cur)) 
            this.setState({numPrep:[...this.state.numPrep, Number(num1)], input:[]}) //숫자 준비해서 모으고 인풋 비우기
        }


    }
   
    //열심히 하는중!!
    componentDidUpdate(){
        console.log('input:',this.state.input)
        console.log('numPrep:',this.state.numPrep)
        console.log('operators:',this.state.operators)
        console.log('result:', this.state.result)
        let firstCalculation;
        switch(this.state.operators[0]){ //됐다 이히히히히히히히히
            case '+': firstCalculation = this.add(Number(this.state.numPrep[0]))
            break;
            case '-': firstCalculation = this.sub(Number(this.state.numPrep[0]))
            break;
            case '*': firstCalculation = this.mul(Number(this.state.numPrep[0]))
            break;
            case '/': firstCalculation = this.div(Number(this.state.numPrep[0]))
            break;
        }

        if(this.state.numPrep.length > 0 && this.state.input.length > 0){ //첫숫자 받았고 새입력이 들어가면
          console.log('현재 가진 입력 값..', this.state.input)
          let secondNum = this.state.input.reduce((acc,cur) => acc+cur)    //새입력을 정리해서 숫자화
          secondNum = Number(secondNum)                                       //새입력을 정리해서 숫자화
          console.log('이 값을 적용할 예정:', secondNum)
        
          
          let result= firstCalculation(secondNum)  
          console.log('result',result)  //<- 이값이 올라가야돼!
            if(result != this.state.result){ //결과값이 변하면
                  this.setState({result: result})  //업데이트
            }

        }

    }
    
    reset = () =>{
        this.setState({input:[], numPrep:[], operators:[], result:[]})
    }
    
    //최종계산
    getResult = () =>{ // '=' 을 누르면
      
    }



    render(){
        let calculatorStyle={
            'marginTop':'18%',
            'marginLeft':'18%'
        }

        let buttonStyle={
            'height':'70px',
            'width':'70px',
            'fontSize':'25px',
            'border':'solid black 1px',
            'background':'white'
        }

        let buttonStyle2={
            'height':'52.5px',
            'width':'52.5px',
            'fontSize':'25px',
            'border':'solid gray 1px',
            'background':'lightgray'
        }


        return(
            <div style={calculatorStyle}>
                calculator
                <div className="numbers" >
                    <div> {[7,8,9].map((el,i) => <button style={buttonStyle} key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                    <div> {[4,5,6].map((el,i) => <button style={buttonStyle} key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                    <div> {[1,2,3].map((el,i) => <button style={buttonStyle} key={i} value={el} onClick={this.inputValue}>{el}</button>)} </div>
                  
                    <button style={buttonStyle} onClick={this.inputValue} value=".">.</button>
                    <button style={buttonStyle} onClick={this.inputValue} value="0">0</button>
                    <button style={buttonStyle} onClick={this.reset}>c</button>
                </div>

                <div className="operators"> 
                    <div>{['+','-','*','/'].map((el,i)=><button style={buttonStyle2} key={i} value={el}  onClick={this.operator}>{el}</button>)}</div>
                    <button style={buttonStyle2} className="getResult" onClick={this.getResult}>=</button>
                </div>

                <div> {this.state.waitingNum} {this.state.operators}{this.state.input} </div>
           
                <div>
                    equation: {this.state.allEquation}
                </div>
                <div>
                     result: {this.state.result}
                </div>
            
            </div>
        )
    }


}

export default Calculator;